//arr[i] + arr[j] = k
//brotforce approach and optimized code:
class arraydemo{
	public static void main(String[] args){
		int arr[] = new int[]{3,5,2,1,-3,7,8,15,6,13};
		int N = 10;
		int k = 10;
		int count = 0;

		
		for(int i=0; i<N; i++){
			for(int j=0; j<N; j++){
				if((arr[i] != arr[j]) && (arr[i] + arr[j] == k))
					count++;
			}
		}
		System.out.println(count);
	}
}
