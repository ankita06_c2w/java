//to reverse an array with time complexity with O(N) and space complexity O(1i) 
class arrayreverse{
	public static void main(String[] args){
		int arr[]  = new int[]{8,4,1,3,9,2,6,7};
		int N = 8;
		int temp = 0;

		for(int i = 0; i<N/2; i++){
			 temp = arr[i];
			arr[i] = arr[N-i-1];
			arr[N-i-1] = temp;
		
		}
		System.out.print("Reversed array is = ");
		for(int i=0; i<N; i++){
			System.out.print(" "+ arr[i]+" ");
		}

	}
} 
