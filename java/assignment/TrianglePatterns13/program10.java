

import java.util.*;

class program10{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
	
		
		int ch=97;

		for(int i=1;i<=row;i++){
		
			int temp=row+1;

			for(int j=1;j<=i;j++){
				
				if(j%2==0){
				
					System.out.print((char)ch+" ");
					ch++;
				}else{
				
					System.out.print(temp+" ");
			
				}
				temp++;
			
			
			}
			System.out.println();
		}
	}
}
