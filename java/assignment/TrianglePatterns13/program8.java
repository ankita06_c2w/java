

import java.util.*;

class program8{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
	
		
		int temp=1;
		int ch=97;

		for(int i=1;i<=row;i++){
		

			for(int j=1;j<=i;j++){
				
				if(j%2==0){
				
					System.out.print((char)ch+" ");
					ch++;
				}else{
				
					System.out.print(temp+" ");
				}
			
			}
			temp++;
			System.out.println();
		}
	}
}
