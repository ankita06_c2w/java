import java.io.*;

class program4{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Rows & Columns");
		int rows=Integer.parseInt(br.readLine());
		int column=Integer.parseInt(br.readLine());
		int arr[][]=new int[rows][column];

		for(int i=0;i<arr.length;i++){
		
			for(int j=0;j<arr[i].length;j++){
			
				System.out.print("Enter element : ");
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}

		System.out.println("Array elements are :");

		for(int i=0;i<arr.length;i++){
		
			for(int j=0;j<arr[i].length;j++){
					
				System.out.print(arr[i][j]);
			}
			System.out.println();
		}

		System.out.println(arr[1][1]);
		System.out.println(arr[1]);
		System.out.println(arr);




	}
}
