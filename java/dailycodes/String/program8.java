class program8{

	public static void main(String[] args){
	
		//way 1
		String str1="Shashi";
		System.out.println(str1);

		//way 2
		String str2=new String("Ashish");
		System.out.println(str2);

		//way 3
		char str3[]=new char[]{'C','O','R','E','2','W','E','B'};
		System.out.println(str3);
	}
}
