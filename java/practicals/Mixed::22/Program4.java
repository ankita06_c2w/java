import java.util.*;
class Program4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter No. of Rows :- ");
        int row = sc.nextInt();
        for (int i = 1; i<=row; i++) {
            int cap = 64+i;
            int sml = 96+i;
            for (int sp = 1; sp < i; sp++) {
                System.out.print("\t");
            }
            if (row%2 == 1) {
                for (int j = row; j >= i; j--) {
                    System.out.print((char)cap + "\t");
                    cap++;
                }
            }else{
                for (int j = row; j >= i; j--) {
                    System.out.print((char)sml + "\t");
                    sml++;
                }
            }
            System.out.println();
        }
    }
}