import java.util.*;
class Program9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter No. of Rows :- ");
        int row = sc.nextInt();
        for (int i = 1; i<=row; i++) {
            int num = row*2-1;
            for (int sp = 1; sp <= row-i; sp++) {
                System.out.print("\t");
                num--;
            }
            for (int j = i*2-1; j >= 1; j--) {
                System.out.print(num + "\t");
                num--;
            }
            System.out.println();
        }
    }
}
