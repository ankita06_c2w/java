import java.util.*;
class Program10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter No. of Rows:- ");
        int row = sc.nextInt();
        for(int i=1; i<=row; i++){
            int num = row;
            int ch = 64+row;
            for(int j=1; j<=row; j++){
                if(i%2==1){
                    if (j%2==1) {
                        System.out.print(num + "\t");
                    }else{
                        System.out.print((char)ch + "\t");
                    }
                    ch--;
                    num--;
                }else{
                    System.out.print((char)ch +  "\t");
                    ch--;
                }
            }
            System.out.println();
        }
    }
}   