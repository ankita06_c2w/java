import java.util.*;
class Program4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter No. of Rows:- ");
        int row = sc.nextInt();
        int num = row;
        int ch = 64+row;
        for(int i=1; i<=row; i++){
            for(int j=1; j<=row; j++){
                if (j==1) {
                    System.out.print((char)ch + "\t");
                }
                else{
                    System.out.print(num + "\t");
                }
                ch++;
                num++;
            }
            System.out.println();
        }
    }
}   