import java.util.*;
class Program8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter No. of Rows:- ");
        int row = sc.nextInt();
        int num = 1;
        for(int i=1; i<=row; i++){
            int ch=64+row;
            for(int j=1; j<=row; j++){
                if ((i+j)%2==0) {
                    System.out.print("#" + "\t");
                }
                else{
                    System.out.print((char)ch-- + "\t");
                }
                num++;
            }
            System.out.println();
        }
    }
}   