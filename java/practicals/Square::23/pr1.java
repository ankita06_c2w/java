import java.util.*;

class Square{

	public static void main(String[] args){
		
		System.out.println("Enter no. of rows: ");
		Scanner sc = new Scanner(System.in);
		int rows = sc.nextInt();
		
		int cp = 64+rows;
		int sm = 96+rows;

		for(int i=1;i<=rows;i++){
			for (int j=1;j<=rows;j++){
				if(j==1){
					System.out.print((char)cp + " ");
				}else{
					System.out.print((char)sm + " ");
				}
				cp++;
				sm++;

			}System.out.println();
		}

	}
}

