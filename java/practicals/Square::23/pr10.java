import java.util.*;

class Square{

        public static void main(String[] args){

                System.out.println("Enter no. of rows: ");
                Scanner sc = new Scanner(System.in);
                int rows = sc.nextInt();
                int num=rows*rows;

                for(int i=1;i<=rows;i++){
                        for (int j=1;j<=rows;j++){
                                if(i==j){    
                                    System.out.print("$" + " ");
				}
                                else{
                                        System.out.print(num*j + " ");
                                }
                                num--;

                        }System.out.println();
                }

        }
}
