/*//IF-ELSE

class Program1IE{

	public static void main(String[] args){
	
		int num=20;

		if(num%2==0){
		
			System.out.println(num+" is an even number");

		}else if(num<=0){
		
			System.out.println(num+"is zero/negative");
		}else{
		
			System.out.println(num+" is an odd number");
		}
	}
}

*/
//SWITCH

class Program1S{

        public static void main(String[] args){

                int num=21;

                switch(num%2){

                       case 0:
			       System.out.println(num+" is an even number");
			       break;

			case 1:
			       System.out.println(num+" is an odd number");
			       break;

			default:
			       System.out.println("Invalid Number");
                }
        }
}
