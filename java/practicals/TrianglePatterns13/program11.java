

import java.util.*;

class program11{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
	
		
		int ch=97;
		int temp=1;

		for(int i=1;i<=row;i++){
	

			for(int j=1;j<=i;j++){
				
				if(i%2==0){
				
					System.out.print((char)ch+" ");
				}else{
				
					System.out.print(temp+" ");
				}
				temp++;
				ch++;
			
			}
			System.out.println();
		}
	}
}
