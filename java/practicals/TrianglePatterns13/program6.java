
import java.util.*;

class program6{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
	
		
		int ch=65+row;

		for(int i=1;i<=row;i++){
		
		
			for(int j=1;j<=i;j++){
				
				System.out.print((char)ch+" ");
				ch++;
			
			}
			System.out.println();
		}
	}
}
