import java.util.*;

class program9{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
	
		
		char ch='c';

		for(int i=1;i<=row;i++){
			

			for(int j=1;j<=i;j++){
				
				if(j%2==0){
					
					System.out.print(ch+" ");
					ch+=2;
					
				}else{
				
					System.out.print(j+" ");
				}
			
			}
			System.out.println();
		}
	}
}
