import java.util.*;
class p1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size = ");
		int size = sc.nextInt();
		int arr[] = new int [size];

		System.out.print("Enter elements");
		for(int i=0; i<size;i++){
			arr[i]=sc.nextInt();
		}
		int sum=0;
		for(int i=0;i<size;i++){
			sum=sum+arr[i];
		}
		int avg = sum/size;
		System.out.println("Array elements average is : "+avg);
	}
}
