import java.util.*;
class p5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size = ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.print("Enter elements = ");
		for(int i=0; i<size; i++){
			arr[i]=sc.nextInt();
		}
		int temp = 0;

		for(int i=0; i<size/2; i++){
			temp = arr[i];
			arr[i] = arr[size-1-i];
			arr[size-1-i] = temp;
		}

		System.out.print("Reveresed array is = ");
		for(int i=0; i<size; i++){
			System.out.print(arr[i]+" ");
		}
	}
}
