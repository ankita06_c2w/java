import java.util.*;
class Program1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Size of Array - ");
        int size = sc.nextInt();
        int flag=0;
        int arr[] = new int[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }
        int temp = arr[0];
        for (int i = 0; i < arr.length-1; i++) {
            if (temp >= arr[i]) {
                temp = arr[i];
            }else{
                flag = 1;
                break;
            }
        }
        if (flag ==1) {
            System.out.println("Array is Not in Decending Order");
        }else{
            System.out.println("Array is in Decending Order");
        }
    }
}