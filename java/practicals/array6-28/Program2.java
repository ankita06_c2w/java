import java.util.*;
class Program2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Size of Array - ");
        int size = sc.nextInt();
        int sum = 0;
        int totCnt = 0;
        int arr[] = new int[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }
        for (int i = 0; i < arr.length; i++) {
            int cnt=0;
            for (int j = 1; j <= arr[i]; j++) {
                if (arr[i]%j == 0) {
                    cnt++;
                }
            }
            if (cnt == 2) {
                sum += arr[i];
                totCnt++;
            }
        }
        System.out.println("Sum of all Prime Numbers is " + sum);
        System.out.println("Total Conunt of Prime Numbers is " + totCnt);
    }
}