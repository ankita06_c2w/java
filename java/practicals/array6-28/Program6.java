import java.util.*;
class Program6 {
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        System.out.print("Enter Size of Array - ");
        int size1 = sc.nextInt();
        int arr1[] = new int[size1];
		int flag=0;
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = sc.nextInt();
        }
		System.out.print("Enter Value - ");
        int val = sc.nextInt();
		for(int j=0; j<arr1.length; j++){
		   if (arr1[j]%val == 0) {
			System.out.println("An Element multiple of "+val+" found at Index "+j);
			flag=1;
		   }
		}
		if (flag==0) {
			System.out.println("Element Not Found");
		}
	}
}
