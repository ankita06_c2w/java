import java.util.*;
class Program7 {
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        System.out.print("Enter Size of Array - ");
        int size1 = sc.nextInt();
        int arr1[] = new int[size1];
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = sc.nextInt();
        }
		System.out.println("\nNew Array ");
		for(int j=0; j<arr1.length; j++){
		   if (arr1[j]>=65 && arr1[j]<=90) {
			System.out.println((char)arr1[j]);
		   }else {
			System.out.println(arr1[j]);
		   }
		}
	}
}
