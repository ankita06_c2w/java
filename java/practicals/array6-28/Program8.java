import java.util.*;
class Program8 {
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        System.out.print("Enter Size of Array - ");
        int size1 = sc.nextInt();
        char arr1[] = new char[size1];
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = sc.next().charAt(0);
        }
		
		for(int j=0; j<arr1.length/2; j++){
			char temp=arr1[j];
			arr1[j]=arr1[arr1.length-j-1];
			arr1[arr1.length-j-1]=temp;
		}
		System.out.println("\nNew Array Will be");
		for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + " ");
        }
	}
}
