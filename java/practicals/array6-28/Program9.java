import java.util.*;
class Program9 {
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        System.out.print("Enter Size of Array - ");
        int size1 = sc.nextInt();
        int arr1[] = new int[size1];
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = sc.nextInt();
        }
		
        int cnt=0;
		for(int i=0; i<arr1.length; i++){
            int rev = 0;
			int temp = arr1[i];
            while (arr1[i]>0) {
                int rem = arr1[i]%10;
                arr1[i] /=10;
                rev = rem + rev*10;
            }
            if (rev == temp) {
                cnt++;
            }
		}
		System.out.println("\nCount of Palindrome in Array is "+cnt);

    }
}