
import java.util.*;

class program10{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows size and columns size of an array");
		int arrRow=sc.nextInt();
		int arrCol=sc.nextInt();

		int arr[][]=new int[arrRow][arrCol];

		System.out.println("Enter array elements");
		for(int i=0;i<arrRow;i++){
	 	
	 		for(int j=0;j<arrCol;j++){
			
				arr[i][j]=sc.nextInt();
			}
		}


		 System.out.println("Output");

		 System.out.println(arr[0][0]);
		 System.out.println(arr[0][arrCol-1]);
		 System.out.println(arr[arrRow-1][0]);
		 System.out.println(arr[arrRow-1][arrCol-1]);
		


		 
	}
}
