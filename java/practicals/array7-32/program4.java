import java.util.*;

class program4{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows size and columns size of an array");
		int arrRow=sc.nextInt();
		int arrCol=sc.nextInt();

		int arr[][]=new int[arrRow][arrCol];

		System.out.println("Enter array elements");
		for(int i=0;i<arrRow;i++){
		
			for(int j=0;j<arrCol;j++){
			
				arr[i][j]=sc.nextInt();
			}
		}


		 System.out.println("Output");


		 for(int i=0;i<arrRow;i++){

			 int sum=0;	 
		
			 for(int j=0;j<arrCol;j++){   

				sum=sum+arr[i][j];	

			  }

		 if((i+1)%2!=0){
		 	System.out.println("Sum of row "+(i+1)+" in array is "+sum);
		 }
		 
		 }


		 
	}
}
