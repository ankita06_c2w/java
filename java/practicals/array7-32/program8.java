
import java.util.*;

class program8{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows size and columns size of an array");
		int arrRow=sc.nextInt();
		int arrCol=sc.nextInt();

		int arr[][]=new int[arrRow][arrCol];

		System.out.println("Enter array elements");
		for(int i=0;i<arrRow;i++){
	 	
	 		for(int j=0;j<arrCol;j++){
			
				arr[i][j]=sc.nextInt();
			}
		}


		 System.out.println("Output");


		 int sum=0;

		 for(int i=0;i<arrRow;i++){

			 for(int j=0;j<arrCol;j++){

			 	if(j==(arrCol-1)-i){
				
					sum=sum+arr[i][j];
				}
			 }
		 }
		 System.out.println("sum of second digonal elements of array is "+sum);


		 
	}
}
