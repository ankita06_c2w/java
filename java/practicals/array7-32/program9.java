
import java.util.*;

class program9{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows size and columns size of an array");
		int arrRow=sc.nextInt();
		int arrCol=sc.nextInt();

		int arr[][]=new int[arrRow][arrCol];

		System.out.println("Enter array elements");
		for(int i=0;i<arrRow;i++){
	 	
	 		for(int j=0;j<arrCol;j++){
			
				arr[i][j]=sc.nextInt();
			}
		}


		 System.out.println("Output");


		 int sum1=0;
		 int sum2=0;

		 for(int i=0;i<arrRow;i++){

			 for(int j=0;j<arrCol;j++){

			 	if(j==(arrCol-1)-i){
				
					sum1=sum1+arr[i][j];
				}

				if(i==j){
				
					sum2=sum2+arr[i][j];
				}
			 }
		 }
		 System.out.println("product of sum of primary and secondary digonal elements of array is "+(sum1*sum2));


		 
	}
}
