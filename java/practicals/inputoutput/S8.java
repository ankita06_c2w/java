import java.util.*;
class S8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Row = ");
		int row = sc.nextInt();

		int no = 64 + row;
		char ch = (char)no;
		int num = 1;
		for (int i = 1; i<=row ; i++){
			for (int j = 1; j<=row ;j++){
				System.out.print(" "+ ch+num + " ");
				num++;
			}
			System.out.println();
		}
	}
}
