// when sum of addition of factors of number = number , then it is called as perfect no.
import java.util.*;
class p1{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number = ");
		int num = sc.nextInt();
		int sum = 0;

		for(int i =1; i<num; i++){
			if(num%i == 0)
				sum = sum+i;

		}

		if(sum == num)
			System.out.println(num +" is a perfect number ");
		else
			
			System.out.println(num +" is not a perfect number");

	}
}
