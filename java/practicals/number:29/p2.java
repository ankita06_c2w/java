// when sum of  factorials of each digit in number is equal to the number then it is called as strong number.
import java.util.*;
class p2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number = ");
		int num = sc.nextInt();

		int sum = 0;
		int temp = num;
		while(num>0){
			int dig = num%10;
			int mult = 1;
		       while(dig>0){

			       mult = mult*dig;
			       dig--;
		       
		       }
	        num/=10;
		sum = sum+mult;	       
		}
		if(sum == temp)
			System.out.println(temp +" is a strong number");
		else

			System.out.println(temp +" is not a strong number");

	}


}
