import java.util.*;
class pyramid10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Row = ");
		int row = sc.nextInt();

		
		for(int i=1;i<=row;i++){
			int no = 64+(row-i+1);
			char ch = (char)no;
			for(int space=1;space<=row-i;space++){
				System.out.print("\t");
			}
			for(int j=1;j<=i*2-1;j++){
				if(j<i){
				System.out.print(ch+"\t");
				ch++;
				
				}else{
					System.out.print(ch+"\t");
					ch--;
			
				}
				
			}
			System.out.println();
			
		}
	}
}
