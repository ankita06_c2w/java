import java.util.*;
class pyramid4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Row = ");
		int row = sc.nextInt();

		char ch  = 'A';
		for(int i=1;i<=row;i++){
			for(int space=1;space<=row-i;space++){
				System.out.print("\t");
			}
			for(int j=1;j<=i*2-1;j++){
				System.out.print(ch+"\t");
				
			}
			System.out.println();
			ch++;
		}
	}
}
