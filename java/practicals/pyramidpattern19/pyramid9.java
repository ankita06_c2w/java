import java.util.*;
class pyramid9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Row = ");
		int row = sc.nextInt();

		
		for(int i=1;i<=row;i++){
		
			char ch1 = 'A';
			char ch2 = 'a';
			for(int space=1;space<=row-i;space++){
				System.out.print("\t");
			}
			for(int j=1;j<=i*2-1;j++){
				if(i%2==1){
				System.out.print(ch1+"\t");
				ch1++;
				
				}else{
					System.out.print(ch2+"\t");
					ch2++;
			
				}
				
			}
			System.out.println();
			
		}
	}
}
