import java.util.*;
class p3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows = ");
		int row = sc.nextInt();
		int col = 0;
		for(int i=0; i<=row*2-1; i++){
			int num = i;
			if(i<row){
				col = i;
			}else{
				col = row*2-i;
			}
			for(int j=0; j<col; j++){
				System.out.print( num+"\t" );
				num--;
			}
		
		System.out.println();
		}
	} 
}
