import java.util.*;
class sp6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Row = ");
		int row = sc.nextInt();
		int num = row;

		for(int i=1; i<=row; i++){
			for(int space=1; space<=i-1; space++){
				System.out.print(" ");
			}
			for(int j=1; j<=row-i+1; j++){
				System.out.print(num);
			}
			System.out.println();
			num--;
		}
	}
}
