
import java.io.*;
class br10{
	public static void main(String[] args) throws IOException{
		BufferedReader ip = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows = ");
		int row = Integer.parseInt(ip.readLine());
		int no = row;
		for (int i= 1; i<=row ; i++){
			int num = 64+i;
			char ch = (char)num;
		
			for (int j=1 ; j<=row-i+1; j++){
				if (no%2!=0 && j%2==0 || no%2==0 && j%2!=0){
					
						System.out.print((int)ch + " ");
					} else{
						System.out.print(ch + " ");
					}
				ch++;
				
			}

			System.out.println();
			
			no--;	
		}
	}
}
