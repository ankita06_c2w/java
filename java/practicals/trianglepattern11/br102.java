import java.io.*;
class br102{
	public static void main(String[] args)throws IOException{
		BufferedReader ip = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter numberof rows = ");
		int row = Integer.parseInt(ip.readLine());
		for(int i = 1; i<=row; i++){
			int no = 64+i;
			char ch = (char)no;

			for (int j=row-i+1;j>=1;j--){
				if((i+j)%2==0 && i%2!=0 || (i+j)%2!=0 && i%2==0){
					System.out.print(ch + " ");
				}else{
					System.out.print((int)ch + " ");
				
				}
				ch++;
			}
			System.out.println();
		}
	}
}


