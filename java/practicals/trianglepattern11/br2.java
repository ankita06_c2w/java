import java.io.*;
class br3{
	public static void main(String[] args) throws IOException{
		BufferedReader ip = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows = ");
		int row = Integer.parseInt(ip.readLine());

		for (int i= 1; i<=row ; i++){
			int num = row;
			for (int j=1 ; j<=i; j++){
				System.out.print(num + " ");
				num--;
			}
			System.out.println();
		}
	}
}


