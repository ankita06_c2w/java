
import java.io.*;
class br6{
	public static void main(String[] args) throws IOException{
		BufferedReader ip = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows = ");
		int row = Integer.parseInt(ip.readLine());

		int num = row;
		for (int i= 1; i<=row ; i++){
			
			for (int j=1 ; j<=row-i+1; j++){
				System.out.print(num + " ");
			
			}
			System.out.println();
			num--;
		}
	}
}
