
import java.io.*;
class br9{
	public static void main(String[] args) throws IOException{
		BufferedReader ip = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows = ");
		int row = Integer.parseInt(ip.readLine());

		for (int i= 1; i<=row ; i++){
			int no = 64+row;
			char ch = (char)no;
			for (int j=1 ; j<=row-i+1; j++){
				System.out.print(ch + " ");
				ch--;
			}
			System.out.println();
		}
	}
}
