
class w4{
	public static void main(String[] args){
		int num = 256985;
		
		while(num>0){
			int digit = num%10;
			if(digit%2!=0){
				System.out.print(" "+ digit*digit +" ");
			}
			num = num/10;
		}
	}
}
