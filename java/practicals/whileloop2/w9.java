
class w9{
	public static void main(String[] args){
		int num = 2469185;
		int sum = 0;
		int square = 0;

		while(num>0){
			int digit = num%10;
			
			if(digit%2!=0){
				square = digit*digit;
				sum = sum+square;
			}
			num = num/10;
		}
		
		System.out.println("Sum of square of odd digits in given number is "+sum);
	}
}
